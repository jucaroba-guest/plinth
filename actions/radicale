#!/usr/bin/python3
#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Configuration helper for Radicale.
"""

import argparse
import os
import shutil
import subprocess
import tempfile

import augeas

from plinth import action_utils
from plinth.modules import radicale

COLLECTIONS_PATH = '/var/lib/radicale/collections'

CONFIG_FILE = '/etc/radicale/config'

DEFAULT_FILE = '/etc/default/radicale'


def parse_arguments():
    """Return parsed command line arguments as dictionary."""
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='subcommand', help='Sub command')

    subparsers.add_parser('setup', help='Setup Radicale configuration')
    subparsers.add_parser('migrate', help='Migrate config to radicale 2.x')
    subparsers.add_parser('enable', help='Enable Radicale service')
    subparsers.add_parser('disable', help='Disable Radicale service')
    configure = subparsers.add_parser('configure',
                                      help='Configure various options')
    configure.add_argument('--rights_type',
                           help='Set the rights type for radicale')

    subparsers.required = True
    return parser.parse_args()


def subcommand_setup(_):
    """Setup Radicale configuration."""
    current_version = radicale.get_package_version()
    if not current_version:
        print('Warning: Unable to get radicale version.')

    aug = load_augeas()

    if current_version and current_version < radicale.VERSION_2:
        aug.set('/files' + DEFAULT_FILE + '/ENABLE_RADICALE', 'yes')
        aug.set('/files' + CONFIG_FILE + '/server/hosts',
                '127.0.0.1:5232, [::1]:5232')
        aug.set('/files' + CONFIG_FILE + '/server/base_prefix', '/radicale/')
        aug.set('/files' + CONFIG_FILE + '/well-known/caldav',
                '/radicale/%(user)s/caldav/')
        aug.set('/files' + CONFIG_FILE + '/well-known/carddav',
                '/radicale/%(user)s/carddav/')
        aug.set('/files' + CONFIG_FILE + '/auth/type', 'remote_user')
        aug.set('/files' + CONFIG_FILE + '/rights/type', 'owner_only')

    aug.save()

    subcommand_enable(None)


def subcommand_migrate(_):
    """Migrate from radicale 1.x to 2.x."""
    current_version = radicale.get_package_version()

    # Migrate data from radicale 1.x to radicale 2.x format.
    if current_version and current_version < radicale.VERSION_2:
        with tempfile.TemporaryDirectory() as temp_directory:
            export_location = os.path.join(temp_directory, 'radicale-export')
            subprocess.run(['radicale', '--export-storage', export_location],
                           check=True)
            collection_root = os.path.join(export_location, 'collection-root')
            shutil.copytree(collection_root,
                            os.path.join(COLLECTIONS_PATH, 'collection-root'))
            subprocess.run(
                ['chown', '-R', 'radicale:radicale', COLLECTIONS_PATH],
                check=True)

    action_utils.webserver_disable('radicale-plinth')


def subcommand_configure(arguments):
    """Sets the radicale rights type to a particular value"""
    current_version = radicale.get_package_version()
    if not current_version:
        print('Warning: Unable to get radicale version.')

    if current_version and current_version >= radicale.VERSION_2:
        if arguments.rights_type == 'owner_only':
            # Radicale 2.x default rights file is equivalent to owner_only.
            arguments.rights_type = 'from_file'

    aug = load_augeas()
    aug.set('/files' + CONFIG_FILE + '/rights/type', arguments.rights_type)
    aug.save()

    if current_version and current_version >= radicale.VERSION_2:
        action_utils.service_try_restart('uwsgi')
    else:
        action_utils.service_try_restart('radicale')


def subcommand_enable(_):
    """Start service."""
    if radicale.get_package_version() >= radicale.VERSION_2:
        # Workaround for bug in radicale's uwsgi script (#919339)
        if not os.path.exists(COLLECTIONS_PATH):
            os.makedirs(COLLECTIONS_PATH)
        action_utils.service_disable('radicale')
        action_utils.uwsgi_enable('radicale')
    else:
        action_utils.service_enable('radicale')
        action_utils.service_restart('radicale')

    action_utils.webserver_enable(radicale.get_web_config())


def subcommand_disable(_):
    """Stop service."""
    action_utils.webserver_disable(radicale.get_web_config())
    if radicale.get_package_version() >= radicale.VERSION_2:
        action_utils.uwsgi_disable('radicale')
    else:
        action_utils.service_disable('radicale')


def load_augeas():
    """Initialize Augeas."""
    aug = augeas.Augeas(
        flags=augeas.Augeas.NO_LOAD + augeas.Augeas.NO_MODL_AUTOLOAD)

    # shell-script config file lens
    aug.set('/augeas/load/Shellvars/lens', 'Shellvars.lns')
    aug.set('/augeas/load/Shellvars/incl[last() + 1]', DEFAULT_FILE)

    # INI file lens
    aug.set('/augeas/load/Puppet/lens', 'Puppet.lns')
    aug.set('/augeas/load/Puppet/incl[last() + 1]', CONFIG_FILE)

    aug.load()
    return aug


def main():
    """Parse arguments and perform all duties."""
    arguments = parse_arguments()

    subcommand = arguments.subcommand.replace('-', '_')
    subcommand_method = globals()['subcommand_' + subcommand]
    subcommand_method(arguments)


if __name__ == '__main__':
    main()
